package classes;

class Constants {
    final static int UP_MOVE    = 1,
                     DOWN_MOVE  = 2,
                     LEFT_MOVE  = 3,
                     RIGHT_MOVE = 4;
}
