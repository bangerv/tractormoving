package interfaces;

import classes.Coordinates;

public interface ConditionMountain {

    Coordinates[] getCoords();
}
