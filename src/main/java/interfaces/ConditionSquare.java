package interfaces;

public interface ConditionSquare {

    double getSide();
}
